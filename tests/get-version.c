#include <stdio.h>
 
extern const char *TIFFGetVersion(void);
 
int main(void)
{
    printf("%s\n", TIFFGetVersion());
    return 0;
}
